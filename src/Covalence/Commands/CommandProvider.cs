﻿using System;
using System.Linq;
using CGSGameManager.Core.Covalence;
using CGSGameManager.Core.Covalence.Commands;

namespace CGSGameManager.Rust.Covalence.Commands
{
    public class CommandProvider : BasicCommandProvider<RustCommand, RustCommandContext>
    {
        public void ProcessConsoleCommand(RustCommand command, ConsoleSystem.Arg arg)
        {
            if (command == null || arg == null)
                return;

            if (arg.Connection?.CgsPlayer != null)
            {
                var player = (RustUser)arg.Connection.CgsPlayer;
                player._lastCommand = CommandType.Console;
            }

            var context = CreateContext(arg.Connection?.CgsPlayer ?? RustServerPlayer.Instance, command, arg);

            try
            {
                command.Call(context);
            }
            catch (Exception e)
            {
                Interface.Core.LogException($"[CMD: {command.FullName}] Failed to call", e);
            }
            finally
            {
                context.Dispose();
            }
        }

        // TODO : Fix Substring error
        public void ProcessChatCommand(string command, string[] args, ConsoleSystem.Arg arg)
        {
            if (string.IsNullOrWhiteSpace(command) || arg == null)
                return;

            var player = arg.Connection?.CgsPlayer ?? RustServerPlayer.Instance;

            if (player is RustUser user)
                user._lastCommand = CommandType.Chat;

            var category = "cgs";

            if (command.Contains("."))
            {
                var index = command.LastIndexOf('.');

                category = command.Substring(0, index).ToLowerInvariant();
                command = command.Substring(index + 1, (command.Length - index) - 1);
            }

            command = command.ToLowerInvariant();
            var cmd = default(RustCommand);

            foreach (var c in _commands.Values)
            {
                foreach (var cc in c)
                {
                    if (cc.Category != category)
                        continue;

                    if (!cc.Aliases.Contains(command))
                        continue;
                    cmd = cc;
                    break;
                }

                if (cmd != null)
                    break;
            }

            if (cmd == null)
                return;
            var context = CreateContext(player, cmd, args);
            context.NativeCallback = arg;

            try
            {
                cmd.Call(context);
            }
            catch (Exception e)
            {
                Interface.Core.LogException($"[CMD: {cmd.FullName}] Failed to call", e);
            }
            finally
            {
                context.Dispose();
            }
        }

        private RustCommandContext CreateContext(ICgsPlayer player, RustCommand command, ConsoleSystem.Arg arg)
        {
            if (player == null || command == null || arg == null)
                return null;
            return new RustCommandContext(player, command, arg);
        }

        protected override RustCommandContext CreateContext(ICgsPlayer player, RustCommand command, string[] args)
        {
            if (player == null || command == null)
                return null;

            return new RustCommandContext(player, command, args);
        }

        protected override RustCommand CreateCommand(string category, string[] aliases, CommandCallback callback, string description)
        {
            return new RustCommand(category, aliases, callback, description);
        }
    }
}
