﻿using CGSGameManager.Core;
using CGSGameManager.Core.Covalence.Commands;
using CGSGameManager.Core.Extensions;
using CGSGameManager.Rust.Covalence;
using CGSGameManager.Rust.Covalence.Commands;

namespace CGSGameManager.Rust
{
    public sealed class RustLoader : Extension
    {
        #region Initialization

        public override string ExtensionName => "Rust Experimental";
        public override string ExtensionAuthor => "KahunaElGrande";
        public override VersionNumber ExtensionVersion => VersionNumber.CgsCurrent();

        public RustLoader(ExtensionManager manager) : base(manager)
        {
        }

        protected override void Load()
        {
            //GameManager = new RustGameManager();
            Core.CompoundLogger.RegisterLogger(new UnityLogger());
            RegisterPlugin("RustCore", new RustCore());
        }

        protected override void ServerInitialized()
        {
            ServerMgr.Instance.gameObject.AddComponent<UnityManager>();
            Interface.Core.RegisterCovalence<CovalenceProvider>().Create();
            DefaultCommandList.Register();
            RustCommands.Register();
        }

        #endregion
    }
}
