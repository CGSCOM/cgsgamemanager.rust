﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CGSGameManager.Core.Covalence;
using CGSGameManager.Core.Covalence.Generic;
using CGSGameManager.Rust.Covalence.Commands;

namespace CGSGameManager.Rust.Covalence
{
    public class CovalenceProvider : ICovalenceProvider<CommandProvider, PlayerManager, RustServer>
    {
        public uint ClientAppId => 252490;

        public uint ServerAppId => 258550;

        public string GameName => "Rust Experimental";

        public CommandProvider Commands { get; private set; }

        public PlayerManager Players { get; private set; }

        public RustServer Server { get; private set; }

        public IServerManager GetServer() => Server;

        public IPlayerManager GetPlayerManager() => Players;

        public ICommandProvider GetCommandProvider() => Commands;

        private bool initialized = false;
        public void Create()
        {
            if (initialized) return;
            initialized = true;
            Commands = new CommandProvider();
            Server = new RustServer();
            Players = new PlayerManager();
        }

        public void Dispose()
        {
            if (!initialized)
                return;
            initialized = false;
            Commands = null;
            Players = null;
            Server = null;
        }
    }
}
