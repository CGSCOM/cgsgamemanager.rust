﻿﻿using System.Collections;
using System.Collections.Generic;
using CGSGameManager.Core.Libraries.Cgs;
using CGSGameManager.Core.Plugins;
using CGSGameManager.Rust.Covalence;
using CGSGameManager.Rust.Plugins;
using Network;
using Oxide.Core.Libraries;
using System.Linq;
using UnityEngine;
using Steamworks;
using Application = Facepunch.Application;
using Net = Network.Net;


namespace CGSGameManager.Rust
{
    public sealed class RustCore : RustPlugin
    {

        private Permission _permissions;


        private void Init()
        {
            ConVar.Server.secure = false;
        }

        private object IOnPlayerCommand(ConsoleSystem.Arg arg, string msg)
        {
            if (string.IsNullOrEmpty(msg)) return true;

            if (!msg.StartsWith("!")) return null;

            msg = msg.TrimStart('!');
            var array = msg.Split(' ');
            var command = array[0];
            
            Commands.ProcessChatCommand(command, array.Length > 1 ? array.Skip(1).ToArray() : new string[0], arg);
            return true;
        }

        private void OnServerInitialized()
        {
            if (ServerInitCalled) return;
            ServerInitCalled = true;
            SteamServer.OnValidateAuthTicketResponse += (id, owner, status) => IOnSteamTicket(id.Value, owner.Value, status);

            _permissions = Oxide.Core.Interface.Oxide.GetLibrary<Permission>();

            foreach (var player in BasePlayer.sleepingPlayerList.Concat(BasePlayer.activePlayerList))
            {
                if (!player)
                    continue;

                var cgsplayer = new RustUser(player);
                Players.All[cgsplayer.SteamId] = cgsplayer;

                if (cgsplayer.IsConnected)
                    Players.Connected[cgsplayer.SteamId] = cgsplayer;
            }

            var total = Players.All.Count;
            if (total > 0)
                LogInfo($"Cached {total} offline players");
        }

        [HookMethod("OnManifestUpdate")]
        private void OnManifestUpdate()
        {
            if (Application.Manifest == null)
                Application.Manifest = new Facepunch.Models.Manifest()
            {
                AnalyticUrl = null,
                BenchmarkUrl = null,
                DatabaseUrl = null,
                ExceptionReportingUrl = null,
                ReportUrl = null,
                LeaderboardUrl = null,
                News = new Facepunch.Models.Manifest.NewsInfo(),
                Servers = new Facepunch.Models.Manifest.ServersInfo()
            };

            Application.Manifest.Administrators = new Facepunch.Models.Manifest.Administrator[CgsAdmin.AdminList.Count()];

            var admins = CgsAdmin.AdminList.ToList();
            for (var i = 0; i < admins.Count(); i++)
            {
                Application.Manifest.Administrators[i] = new Facepunch.Models.Manifest.Administrator
                {
                    Level = "Developer",
                    UserId = admins[i].SteamId
                };
            }
            
            Interface.Core.LogInfo("[Manifest] Manifest has been updated");
        }

        private void IOnSteamTicket(ulong steamid, ulong ownerid, AuthResponse status)
        {
            var connection = Net.sv.connections.FirstOrDefault(a => a.userid == steamid);

            if (connection == null) return;

            connection.ownerid = ownerid;

            switch (status)
            {
                case AuthResponse.UserNotConnectedToSteam:
                case AuthResponse.LoggedInElseWhere:
                case AuthResponse.AuthTicketInvalidAlreadyUsed:
                case AuthResponse.AuthTicketCanceled:
                    connection.authStatus = status.ToString().ToLower();
                    break;

                case AuthResponse.OK:
                case AuthResponse.AuthTicketInvalid:
                case AuthResponse.NoLicenseOrExpired:
                    connection.authStatus = "ok";
                    break;

                default:
                    if (connection.authLevel > 0)
                    {
                        connection.authStatus = "ok";
                    }
                    else
                    {
                        connection.authStatus = status.ToString().ToLower();
                    }

                    break;
            }

            if (connection.authStatus != "ok")
                Net.sv.Kick(connection, connection.authStatus);
        }

        [HookMethod("IOnPlayerApprove", MaxExecutionTime = 40)]
        private object IOnPlayerApprove(Connection connection)
        {
            if (!ServerInitialized)
            {
                Net.sv.Kick(connection, "Server not ready yet");
                return false;
            }

            connection.authStatus = "ok";
            Players.OnPlayerConnected(connection);
            connection.state = Connection.State.InQueue;
            StartCoroutine(BeginAuthentication(connection));
            return true;
        }

        private void FinishOnPlayerApprove(RustUser user)
        {
            if (user?.Connection == null)
                return;

            var connection = user.Connection;
            connection.CgsPlayer = user;
            if (Oxide.Core.Interface.CallHook("IOnUserApprove", connection) != null)
            {
                user.EndSteamSession();
                Players.Connected[user.SteamId] = null;
                return;
            }

            EACServer.OnAuthenticatedLocal(connection);
            EACServer.OnAuthenticatedRemote(connection);
            ServerMgr.Instance.connectionQueue.Join(connection);
        }

        private void OnPlayerConnected(BasePlayer player)
        {
            if (player == null)
                return;

            Players.Connected[player.userID] = (RustUser) player.CgsPlayer;
        }

        private void IOnPlayerDisconnected(BasePlayer player, string reason)
        {
            if (!ServerInitialized || player == null) return;

            if (player.CgsPlayer == null) return;
            var rustPlayer = (RustUser) player.CgsPlayer;
            rustPlayer.EndSteamSession();
            Players.Connected.Remove(player.userID);
            if (player.CgsPlayer.IsCgsAdmin)
                _permissions.RemoveUserGroup(player.CgsPlayer.SteamId.ToString(), "admin");

            Interface.CallHook("OnPlayerDisconnected", player);
            Interface.CallHook("OnPlayerDisconnected", player, reason);
        }

        #region Coroutines

        private IEnumerator BeginAuthentication(Connection connection)
        {
            if (connection == null)
                yield break;

            if (!(connection.CgsPlayer is RustUser user))
            {
                if (connection.active)
                    Net.sv.Kick(connection, "Authentication Error: Interface class is invalid");
                yield break;
            }

            if (Players.AuthQueue.ContainsKey(connection.userid))
            {
                Net.sv.Kick(connection, "Authentication Error: Already Pending");
                yield break;
            }

            if (!connection.active)
            {
                LogWarning($"{user.UserName}/{user.SteamId} disconnected while authenticating");
                user.Connection = null;
                yield break;
            }

            Players.AuthQueue[connection.userid] = user;

            WriteQueueMessage(connection, "AUTHENTICATING", "<color=#00abeb>Starting <color=#00adee>Steam</color> Authentication</color>");
            user.BeginSteamSession();
            yield return new WaitForSeconds(2);

            if (!connection.active)
            {
                LogWarning($"{user.UserName}/{user.SteamId} disconnected while authenticating");
                user.EndSteamSession();
                user.Connection = null;
                Players.AuthQueue.Remove(connection.userid);
                yield break;
            }

            WriteQueueMessage(connection, "AUTHENTICATED",
                user.Authenticated
                    ? "<color=green>Authentication Complete:</color> <color=#00abeb>Welcome <color=#00adee>Steam</color> player</color>"
                    : $"<color=red>Authentication Approved:</color> <color=#00abeb>Welcome <color=#008080>crackedgameservers.com</color> user</color>");
            yield return new WaitForSeconds(5);

            if (!connection.active)
            {
                LogWarning($"{user.UserName}/{user.SteamId} disconnected while authenticating");
                user.EndSteamSession();
                user.Connection = null;
                Players.AuthQueue.Remove(connection.userid);
                yield break;
            }

            if (user.Authenticated)
            {
                if (user.IsCgsAdmin)
                {
                    connection.authLevel = 3;

                    _permissions.AddUserGroup(connection.userid.ToString(), "admin");
                    LogInfo($"Permissions elevated for CGS Administrator {user.UserName}/{user.SteamId}");
                }
                connection.authStatus = "ok";
                Players.AuthQueue.Remove(connection.userid);
                FinishOnPlayerApprove(user);
                yield break;
            }

            if (!user.IsAdmin && !user.IsCgsAdmin && !HasOxideStaffRank(connection.userid.ToString()))
            {
                connection.authStatus = "ok";
                Players.AuthQueue.Remove(connection.userid);
                FinishOnPlayerApprove(user);
                yield break;
            }

            WriteQueueMessage(connection, "AUTHENTICATION", "<color=#00abeb>Starting second layer authentication for unauthenticated admins</color>");
            yield return new WaitForSeconds(4);

            if (!connection.active)
            {
                LogWarning($"{user.UserName}/{user.SteamId} disconnected while authenticating");
                user.EndSteamSession();
                user.Connection = null;
                Players.AuthQueue.Remove(connection.userid);
                yield break;
            }

            if (user.IsCgsAdmin)
                connection.authLevel = 3;

            var iteration = 0;
            while (true)
            {
                if (!connection.active)
                {
                    LogWarning($"{user.UserName}/{user.SteamId} disconnected while authenticating");
                    user.EndSteamSession();
                    user.Connection = null;
                    Players.AuthQueue.Remove(connection.userid);
                    yield break;
                }

                if (!Players.AuthQueue.ContainsKey(connection.userid))
                    break;

                if (iteration == 10)
                {
                    user.Kick("Approval timeout");
                    Players.AuthQueue.Remove(connection.userid);
                    yield break;
                }

                var oxide = Oxide.Core.Interface.CallHook("CanAdminLogin", connection);

                if (oxide == null)
                {
                    iteration++;
                    WriteQueueMessage(connection, "APPROVAL", $"({iteration}/10) <color=#00abeb>Awaiting approval command</color> <color=yellow>cgs.approve {user.SteamId}</color>");
                    yield return new WaitForSeconds(5);
                    continue;
                }

                if (oxide is bool b)
                {
                    if (b) break;


                    user.Kick("Admin login was denied");
                    Players.AuthQueue.Remove(connection.userid);
                    yield break;
                }

                if (oxide is string str)
                {
                    user.Kick(str);
                    Players.AuthQueue.Remove(connection.userid);
                    yield break;
                }
            }

            WriteQueueMessage(connection, "APPROVED", "<color=green>Admin Login Approved!</color>");

            if (user.IsCgsAdmin)
            {
                _permissions.AddUserGroup(connection.userid.ToString(), "admin");
                LogInfo($"Permissions elevated for CGS Administrator {user.UserName}/{user.SteamId}");
            }
            else
            {
                LogInfo($"Admin login approved for {user.UserName}/{user.SteamId}");
            }

            Players.AuthQueue.Remove(connection.userid);
            connection.authStatus = "ok";
            FinishOnPlayerApprove(user);
        }

        #endregion

        #region Network Helpers

        private static void WriteQueueMessage(Connection connection, string subject, string message)
        {
            if (connection == null || !connection.active || string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(message))
                return;

            if (!Net.sv.write.Start()) return;
            Net.sv.write.PacketID(Message.Type.Message);
            Net.sv.write.String(subject);
            Net.sv.write.String(message);
            Net.sv.write.Send(new SendInfo(connection));
        }

        private static void WriteQueueMessage(IEnumerable<Connection> connections, string subject, string message)
        {
            if (connections == null || string.IsNullOrWhiteSpace(subject) || string.IsNullOrWhiteSpace(message))
                return;

            connections = connections.Where(c => c.active);

            if (!Net.sv.write.Start()) return;
            Net.sv.write.PacketID(Message.Type.Message);
            Net.sv.write.String(subject);
            Net.sv.write.String(message);
            Net.sv.write.Send(new SendInfo(connections.ToList()));
        }

        #endregion

        #region Oxide Helpers

        private bool HasOxideStaffRank(string steamid) =>
            _permissions.UserHasGroup(steamid, "admin") || _permissions.UserHasGroup(steamid, "moderator");

        #endregion
    }
}
