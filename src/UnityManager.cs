﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;
using CGSGameManager.Rust.Covalence;
using Facepunch;
using Rust;
using UnityEngine;
using Logger = CGSGameManager.Core.Logging.Logger;
using LogType = CGSGameManager.Core.Logging.LogType;

namespace CGSGameManager.Rust
{
    public sealed class UnityManager : MonoBehaviour
    {
        public static UnityManager Instance => ServerMgr.Instance.GetComponent<UnityManager>();

        private Action _onTickCallback;

        private void Awake()
        {
            _onTickCallback = Interface.Core.RegisterTickCallback();

            if (_onTickCallback == null)
            {
                enabled = false;
                GameObject.Destroy(this);
                return;
            }

            enabled = true;
        }

        private void Update()
        {
            _onTickCallback?.Invoke();
        }
    }

    public class UnityLogger : Logger
    {
        internal UnityLogger() : base(true)
        {
            
        }

        protected override void ProcessMessage(LogMessage message)
        {
            switch (message.Type)
            {
                default:
                    Debug.Log(message.ConsoleMessage);
                    break;

                case LogType.Warning:
                    Debug.LogWarning(message.ConsoleMessage);
                    break;

                case LogType.Error:
                    Debug.LogError(message.ConsoleMessage);
                    break;
            }
        }
    }
}
