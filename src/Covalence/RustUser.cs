﻿using CGSGameManager.Core.Covalence;
using Network;
using System;
using System.Globalization;
using System.Net;
using CGSGameManager.Core.Covalence.Players;
using Rust;
using UnityEngine;

namespace CGSGameManager.Rust.Covalence
{
    public sealed class RustUser : CgsPlayer
    {
        #region Properties

        private BasePlayer _basePlayer;
        private Connection _connection;

        public BasePlayer BasePlayer
        {
            get
            {
                if (_basePlayer != null)
                    return _basePlayer;

                if (_connection == null || _connection.player == null) return null;
                BasePlayer = (BasePlayer) _connection.player;
                return _basePlayer;

            }
            internal set
            {
                if (value == null)
                {
                    if (_basePlayer != null)
                        _basePlayer.CgsPlayer = null;
                    _basePlayer = null;
                    return;
                }

                _basePlayer = value;
                _basePlayer.CgsPlayer = this;
            }
        }

        public Connection Connection
        {
            get
            {
                if (_connection != null)
                    return _connection;

                if (_basePlayer == null || _basePlayer.net?.connection == null) return null;
                Connection = _basePlayer.net.connection;
                return _connection;
            }
            internal set
            {
                if (value == null)
                {
                    if (_connection != null)
                        _connection.CgsPlayer = null;

                    _connection = null;
                    return;
                }

                _connection = value;
                _connection.CgsPlayer = this;
            }
        }

        #endregion

        #region Constructors

        internal CommandType _lastCommand;

        private RustUser()
        {
            _lastCommand = CommandType.Chat;
        }

        internal RustUser(Connection connection) : this()
        {
            Connection = connection ??
                         throw new ArgumentNullException(nameof(connection),
                             "Tried to create player from null connection");

            SteamId = connection.userid;
            OwnerId = connection.ownerid;

            if (connection.player != null)
                BasePlayer = (BasePlayer) connection.player;
        }

        internal RustUser(BasePlayer player) : this()
        {
            
            BasePlayer = player ? player : throw new ArgumentNullException(nameof(player), "Tried to create player instance from null connection");
            SteamId = player.userID;

            if (player.net?.connection == null) return;

            Connection = player.net.connection;
            OwnerId = Connection.ownerid;
        }

        #endregion

        #region Information

        /// <inheritdoc />
        public override string UserName
        {
            get => BasePlayer != null ? BasePlayer.displayName : Connection?.username ?? "Blaster D:";
            set
            {
                var name = value?.Trim(' ', '/', '\\', '<', '>') ?? "Blaster :D";
                if (name.Length > 25)
                    name = name.Substring(0, 25);

                if (BasePlayer != null)
                {
                    BasePlayer.displayName = name;
                    if (BasePlayer.IPlayer != null)
                        BasePlayer.IPlayer.Name = name;
                }
                
                if (Connection != null) Connection.username = name;
            }
        }

        /// <inheritdoc />
        public override IPEndPoint Address => GetUserEndpoint();

        /// <inheritdoc />
        public override bool IsAdmin => ServerUsers.Is(SteamId, ServerUsers.UserGroup.Owner) ||
                                        ServerUsers.Is(SteamId, ServerUsers.UserGroup.Moderator);

        /// <inheritdoc />
        public override bool IsConnected => Connection?.active ?? false;

        /// <inheritdoc />
        public override string Language
        {
            get => Connection.info.GetString("global.language", "en");
            set => Connection.info.Set("global.language", value);
        }

        /// <inheritdoc />
        public override CommandType LastCommand => _lastCommand;

        /// <inheritdoc />
        public override uint SessionTime => Connection != null ? (uint)Connection.GetSecondsConnected() : 0;

        #endregion

        #region Administration

        /// <inheritdoc />
        public override void Teleport(float x, float y, float z)
        {
            var vector = new Vector3(x, y, z);

            var withinRender = Vector3.Distance(BasePlayer.transform.position, vector) <= 2000;

            if (IsConnected && !withinRender)
                BasePlayer.ClientRPCPlayer(null, BasePlayer, "StartLoading");

            if (!BasePlayer.IsSleeping() && !withinRender)
            {
                BasePlayer.SetPlayerFlag(BasePlayer.PlayerFlags.Sleeping, true);
                if (!BasePlayer.sleepingPlayerList.Contains(BasePlayer))
                    BasePlayer.sleepingPlayerList.Add(BasePlayer);

                BasePlayer.CancelInvoke("InventoryUpdate");
            }

            BasePlayer.MovePosition(vector);

            if (IsConnected)
            {
                BasePlayer.ClientRPCPlayer(null, BasePlayer, "ForcePositionTo");
                if (!withinRender) BasePlayer.SetPlayerFlag(BasePlayer.PlayerFlags.ReceivingSnapshot, true);
            }

            BasePlayer.SendNetworkUpdate(BasePlayer.NetworkQueue.UpdateDistance);

            if (!IsConnected) return;
            try
            {
                BasePlayer.ClearEntityQueue(null);
            }
            catch
            {
            }
            if (!withinRender) BasePlayer.SendFullSnapshot();
        }

        /// <inheritdoc />
        public override void SendChatMessage(string message)
        {
            if (string.IsNullOrEmpty(message)) return;
            BasePlayer.SendConsoleCommand("chat.add", 76561198337782098, message, 1.0);
        }

        /// <inheritdoc />
        public override void SendMessageAsPlayer(string message)
        {
            if (string.IsNullOrEmpty(message)) return;
            BasePlayer.SendConsoleCommand("chat.say", message);
        }

        /// <inheritdoc />
        public override void Reply(string message)
        {
            if (LastCommand == CommandType.Chat)
                SendChatMessage(message);
            else
                BasePlayer.ConsoleMessage(message);
        }

        /// <inheritdoc />
        protected override Vector3Position GetCurrentPosition()
        {
            var vector = BasePlayer.transform.position;
            return new Vector3Position(vector.x, vector.y, vector.z);
        }

        /// <summary>
        /// Starts a Steam Authenticated Session
        /// </summary>
        public void BeginSteamSession()
        {
            if (Steamworks.SteamServer.BeginAuthSession(Connection.token, SteamId))
            {
                Authenticated = true;
                Steamworks.SteamServer.UpdatePlayer(SteamId, UserName, 1);
                Debug.Log($"User {UserName}/{SteamId} has authenticated via Steam");
                return;
            }

            Debug.LogWarning($"User {UserName}/{SteamId} has failed Steam authentication (expunging Steam session)");
            Steamworks.SteamServer.EndSession(SteamId);
            Authenticated = false;
        }

        /// <summary>
        /// Ends a Steam Authenticated Session
        /// </summary>
        public void EndSteamSession()
        {
            if (!Authenticated)
                return;

            Authenticated = false;
            Steamworks.SteamServer.EndSession(SteamId);
            Debug.Log($"Ended authenticated Steam session for {UserName}/{SteamId}");
        }

        #endregion

        #region Helpers

        private IPEndPoint GetUserEndpoint()
        {
            if (Connection == null)
                return null;

            var ip = Connection.ipaddress;
            var port = 0;

            if (string.IsNullOrWhiteSpace(ip))
                return null;

            if (!ip.Contains(":")) return new IPEndPoint(IPAddress.Parse(ip), port);
            var split = ip.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            ip = split[0];
            port = int.Parse(split[1]);

            return new IPEndPoint(IPAddress.Parse(ip), port);
        }

        #endregion
    }
}
