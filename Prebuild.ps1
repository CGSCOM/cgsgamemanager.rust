param (
    [Parameter(Mandatory=$true)][string]$project,
    [Parameter(Mandatory=$true)][string]$dotnet,
    [Parameter(Mandatory=$true)][string]$managed,
    [string]$appid = "0",
    [string]$branch = "public",
    [string]$depot = ""
)

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$game_name = $project -Replace "CGSGameManager."
if ($depot) { $depot = "-depot $depot" }

$root_dir = $PSScriptRoot
$tools_dir = "$root_dir\tools"
$project_dir = "$root_dir\src"
$deps_dir = "$project_dir\Dependencies"
$patch_dir = "$deps_dir\Patched"
$managed_dir = "$patch_dir\$managed"
$output_dir = "$root_dir\output\$managed"

New-Item "$tools_dir", "$managed_dir", $output_dir -ItemType Directory -Force | Out-Null

if ("$branch" -ne "public" -and (Test-Path "$root_dir\resources\$game_name-$branch.cgspj")) {
    $patch_proj = "$root_dir\resources\$game_name-$branch.cgspj"
} else {
    $patch_proj = "$root_dir\resources\$game_name.cgspj"
}

Write-Host $patch_proj

$project = "$project_dir\$project"

function Find-Dependencies {
    if (!(Test-Path "$project.csproj")) {
        Write-Host "Could not find a .csproj file for $game_name"
        exit 1
        if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
    }

    $csproj = Get-Item "$project.csproj"
    $xml = [xml](Get-Content $csproj)
    Write-Host "Getting references for $branch branch of $appid"

    try {
        $hint_path = "Dependencies\\Patched\\\$\(ManagedDir\)\\"
        ($xml.SelectNodes("//Reference") | Select-Object HintPath -ExpandProperty HintPath | Select-String -Pattern "CGSGameManager" -NotMatch) -Replace $hint_path | Out-File "$tools_dir\.references"
        Add-Content "$tools_dir\.references" "mscorlib.dll"
    } catch {
            Write-Host "Could not get references or none found in $project.csproj"
            if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
            exit 1
    }

    DepotDownloader-Get
}

function DepotDownloader-Get {
    $depot_dll = "$tools_dir\DepotDownloader.dll"
    $env:DEPOTDOWNLOADER = $depot_dll

    if (!(Test-Path "$depot_dll") -or (Get-ChildItem "$depot_dll").CreationTime -lt (Get-Date).AddDays(-7)) {
        Write-Host "Determining latest release of Steam DepotDownloader"

        try {
            $json = (Invoke-WebRequest "https://api.github.com/repos/SteamRE/DepotDownloader/releases" -UseBasicParsing | ConvertFrom-Json)[0]
            $version = $json.tag_name -replace '\w+(\d+(?:\.\d+)+)', '$1'
            $release_zip = $json.assets[0].name
        } catch {
            Write-Host "Could not get Steam DepotDownloader from GitHub"
            Write-Host $_.Exception.Message
            if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
            exit 1
        }

        Write-Host "Downloading version $version of Steam DepotDownloader"

        try {
            Invoke-WebRequest $json.assets[0].browser_download_url -Out "$tools_dir\$release_zip"
        } catch {
            Write-Host "Could not download Steam DepotDownloader from GitHub"
            Write-Host $_.Exception.Message
            if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
            exit 1
        }

        Write-Host "Extracting Steam DepotDownloader release files"
        Expand-Archive "$tools_dir\$release_zip" -DestinationPath "$tools_dir" -Force
    
        if (!(Test-Path "$tools_dir\DepotDownloader.dll")) {
            DepotDownloader-Get
            return
        }

        Remove-Item "$tools_dir\depotdownloader-*.zip"
    } else {
        Write-Host "Recent version of Steam DepotDownloader already downloaded"
    }

    Get-Dependencies
}

function Get-Dependencies {
    try {
		Start-Process dotnet -WorkingDirectory $tools_dir -ArgumentList "DepotDownloader.dll -app $appid -branch $branch $depot -dir $patch_dir -filelist $tools_dir\.references" -NoNewWindow -Wait
    } catch {
        Write-Host "Could not start or complete Steam DepotDownloader process"
        Write-Host $_.Exception.Message
        if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
        exit 1
    }

    Write-Host "Copying latest build of CGSGameManager.Core.dll for $game_name"
    if (!(Test-Path "$tools_dir\CGSGameManager.Core.dll")) {
        try {
            Copy-Item "$root_dir\packages\cgsgamemanager.core\*\lib\$dotnet\CGSGameManager.Core.dll" "$tools_dir" -Force
        } catch {
            Write-Host "Could not copy CGSGameManager.Core.dll to $tools_dir"
            Write-Host $_.Exception.Message
            if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
            exit 1
        }
    }

    Write-Host "Copying latest build of Oxide.Core.dll for $game_name"
    if (!(Test-Path "$tools_dir\Oxide.Core.dll")) {
        try {
            Copy-Item "$root_dir\packages\oxide.core\*\lib\$dotnet\Oxide.Core.dll" "$tools_dir" -Force
        } catch {
            Write-Host "Could not copy Oxide.Core.dll to $tools_dir"
            Write-Host $_.Exception.Message
            if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
            exit 1
        }
    }

    Write-Host "Copying latest build of CGSGameManager.Core.dll for CgsPatcher"
    if (!(Test-Path "$managed_dir\CGSGameManager.Core.dll")) {
        try {
            Copy-Item "$root_dir\packages\cgsgamemanager.core\*\lib\$dotnet\CGSGameManager.Core.dll" "$managed_dir" -Force
        } catch {
            Write-Host "Could not copy CGSGameManager.Core.dll to $managed_dir"
            Write-Host $_.Exception.Message
            if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
            exit 1
        }
    }

    Write-Host "Copying latest build of Oxide.Core.dll for CgsPatcher"
    if (!(Test-Path "$managed_dir\Oxide.Core.dll")) {
        try {
            Copy-Item "$root_dir\packages\oxide.core\*\lib\$dotnet\Oxide.Core.dll" "$managed_dir" -Force
        } catch {
            Write-Host "Could not copy Oxide.Core.dll to $managed_dir"
            Write-Host $_.Exception.Message
            if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
            exit 1
        }
    }

    Get-Patcher
}

function Get-Patcher {
    $patcher_exe = "$managed_dir\CGSPatcher.exe"
    $env:CGSPATCHER = $patcher_exe
    if (!(Test-Path "$patcher_exe") -or (Get-ChildItem "$patcher_exe").CreationTime -lt (Get-Date).AddDays(-7)) {
        Write-Host "Downloading latest build of CgsPatcher"
        $patcher_url = "https://ci.appveyor.com/api/projects/crackedgameservers/cgsgamepatcher-9b9wo/artifacts/CGSPatcher.exe"

        try {
            Invoke-WebRequest $patcher_url -Out "$patcher_exe"
        } catch {
            Write-Host "Could not download CGSPatcher.exe from AppVeyor"
            Write-Host $_.Exception.Message
            if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
            exit 1
        }
    } else {
        Write-Host "Recent build of CGSPatcher already downloaded"
    }

    Get-Oxide
}

function Get-Oxide {
    $oxide = "https://umod.org/games/$game_name/download"
	$release_zip = "Oxide.${game_name}.zip"

        Write-Host "Downloading version of $release_zip"

        try {
            Invoke-WebRequest $oxide -Out "$tools_dir\$release_zip"
        } catch {
            Write-Host "Could not download Steam $release_zip from GitHub"
            Write-Host $_.Exception.Message
            if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
            exit 1
        }

        Write-Host "Extracting $release_zip release files"
        Expand-Archive "$tools_dir\$release_zip" -DestinationPath "$patch_dir" -Force

        Remove-Item "$tools_dir\Oxide-*.zip"
        Start-Patcher
}

function Start-Patcher {
    if (!(Test-Path "$managed_dir\CGSPatcher.exe")) {
        Get-Patcher
        return
    }

    Write-Host "Starting Patcher"
    try {
		Start-Process "$managed_dir\CGSPatcher.exe" -WorkingDirectory $managed_dir -ArgumentList "-console -unflag -dir `"$managed_dir`" -project $patch_proj" -NoNewWindow -Wait
        Copy-ToOutput
    } catch {
        Write-Host "Could not start or complete CGSPatcher process"
        Write-Host $_.Exception.Message
        if ($LastExitCode -ne 0) { $host.SetShouldExit($LastExitCode) }
        exit 1
    }
}

function Copy-ToOutput {
    (Get-ChildItem $managed_dir -Filter "*_Original.dll") | ForEach-Object {
        $name = $_.Name.Replace("_Original", "")
        Write-Host "Copying patched $name to output"
        Copy-Item $_.FullName.Replace("_Original", "") $output_dir -Force
    }
}

Find-Dependencies