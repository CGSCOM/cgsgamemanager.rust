﻿using System.Collections;
using CGSGameManager.Core.Plugins;
using CGSGameManager.Rust.Covalence;
using CGSGameManager.Rust.Covalence.Commands;

namespace CGSGameManager.Rust.Plugins
{
    public abstract class RustPlugin : Plugin
    {
        protected CovalenceProvider Covalence => Interface.Core.CovalenceProvider as CovalenceProvider;

        protected RustServer Server => Covalence?.Server;
        protected PlayerManager Players => Covalence?.Players;
        protected CommandProvider Commands => Covalence?.Commands;

        protected bool ServerInitialized => Interface.Core.ExtensionManager.ServerInitComplete;
        protected bool ServerInitCalled = false;

        protected void StartCoroutine(IEnumerator routine)
        {
            if (UnityManager.Instance == null || routine == null)
                return;

            UnityManager.Instance.StartCoroutine(routine);
            
        }

        protected void StopCoroutine(IEnumerator routine)
        {
            if (UnityManager.Instance == null || routine == null)
                return;

            UnityManager.Instance.StopCoroutine(routine);
        }
    }
}
