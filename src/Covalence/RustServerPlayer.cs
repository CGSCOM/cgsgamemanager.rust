﻿using CGSGameManager.Core.Covalence.Players;
using UnityEngine;

namespace CGSGameManager.Rust.Covalence
{
    public class RustServerPlayer : CgsServerPlayer
    {
        public static readonly RustServerPlayer Instance = new RustServerPlayer();

        public override uint SessionTime => (uint) Time.realtimeSinceStartup;

        public override void SendChatMessage(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return;

            Debug.Log(message);
        }

        public override void Reply(string message)
        {
            SendChatMessage(message);
        }

        public override void SendMessageAsPlayer(string message)
        {
            return;
        }
    }
}
