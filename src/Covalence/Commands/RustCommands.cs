﻿using System.Collections.Generic;
using System.Linq;
using CGSGameManager.Core.Covalence;
using CGSGameManager.Core.Libraries;

namespace CGSGameManager.Rust.Covalence.Commands
{
    public sealed class RustCommands : ICommandList
    {
        public static RustCommands Instance { get; private set; }

        private readonly Translations _translation;
        private readonly PlayerManager _players;

        private RustCommands()
        {
            _translation = Interface.Core.ExtensionManager.GetLibrary<Translations>();
            _players = (PlayerManager) Interface.Core.CovalenceProvider.GetPlayerManager();
            Interface.Core.CovalenceProvider.GetCommandProvider().RegisterCommand("cgs", "approve", AuthApprove,
                "Approve a player inside the authentication queue");
        }

        public static void Register()
        {
            if (Instance != null)
                return;

            Instance = new RustCommands();
            Instance.RegisterMessages();
        }

        public void RegisterMessages()
        {
            _translation.RegisterMessage(new Dictionary<string, string>
            {
                ["Command_ApproveUsage"] = "<color=yellow>Approve Usage:</color>\n" +
                                           "<color=cyan>cgs.approve</color> <color=orange>[SteamId/PlayerName/Index]</color> - <color=grey>Approves a player in the authentication queue</color>\n" +
                                           "<color=cyan>cgs.approve</color> <color=orange>all</color> - <color=grey>Approves all players in the authentication queue</color>\n" +
                                           "<color=cyan>cgs.approve</color> <color=orange>list</color> - <color=grey>Shows the current queue</color>",
                ["Command_ApproveAll"] = "<color=teal><color=yellow>{0}</color> players have been approved</color>",
                ["Command_ApproveSingle"] = "<color=teal><color=red>{0}</color> has been <color=green>approved</color></color>",
                ["Command_ApproveEmpty"] = "<color=teal>Authentication queue is currently empty</color>",
                ["Command_ApproveListTitle"] = "<color=yellow>Current Authentication Queue({0})</color>"
            });
        }

        private void AuthApprove(ICommandContext context)
        {
            if (!context.Player.IsAdmin && !context.Player.IsCgsAdmin)
            {
                context.Reply(GetMessage("NoPermission", context, context.Command));
                return;
            }

            switch (context.Arguments.Length)
            {
                case 0:
                    context.Reply(GetMessage("Command_ApproveUsage", context));
                    return;

                case 1:
                    if (!_players.AuthQueue.Any())
                    {
                        context.Reply(GetMessage("Command_ApproveEmpty", context));
                        return;
                    }

                    if (context.Arguments[0].ToLowerInvariant().Equals("list"))
                    {
                        var message = GetMessage("Command_ApproveListTitle", context, _players.AuthQueue.Count);
                        for (var i = 0; i < _players.AuthQueue.Count; i++)
                        {
                            var user = _players.AuthQueue.Values.ElementAt(i);
                            message +=
                                $"\n<color=teal>[{i}] <color=yellow>{user.SteamId} - {user.UserName}</color></color>";
                        }
                        context.Reply(message);
                        return;
                    }

                    if (context.Arguments[0].ToLowerInvariant().Equals("all"))
                    {
                        var count = _players.AuthQueue.Count;
                        _players.AuthQueue.Clear();
                        context.Reply(GetMessage("Command_ApproveAll", context, count));
                        return;
                    }

                    var lookup = context.Arguments[0].ToLowerInvariant();

                    if (lookup.Length < 17 && int.TryParse(lookup, out var index))
                    {
                        var element = _players.AuthQueue.Values.ElementAtOrDefault(index);

                        if (element == null)
                        {
                            context.Reply(GetMessage("UserNotFound", context, $"index {index}"));
                            return;
                        }

                        _players.AuthQueue.Remove(element.SteamId);
                        context.Reply(GetMessage("Command_ApproveSingle", context, element.UserName));
                        return;
                    }

                    if (lookup.Length == 17 && ulong.TryParse(lookup, out var steamid))
                    {
                        if (!_players.AuthQueue.TryGetValue(steamid, out var element))
                        {
                            context.Reply(GetMessage("UserNotFound", context, $"steamid {steamid}"));
                            return;
                        }

                        _players.AuthQueue.Remove(steamid);
                        context.Reply(GetMessage("Command_ApproveSingle", context, element.UserName));
                        return;
                    }

                    var e = _players.AuthQueue.Values.FirstOrDefault(p =>
                        p.UserName.ToLowerInvariant().Contains(lookup));

                    if (e == null)
                    {
                        context.Reply(GetMessage("UserNotFound", context, $"{lookup}"));
                        return;
                    }

                    _players.AuthQueue.Remove(e.SteamId);
                    context.Reply(GetMessage("Command_ApproveSingle", context, e.UserName));
                    return;

                default:
                    context.Reply(GetMessage("Command_ApproveUsage", context));
                    return;
            }
        }

        /// <inheritdoc />
        public string GetMessage(string key, ICgsPlayer player, params object[] args)
        {
            var message = _translation.GetMessage(key, player);

            if (!message.IsNullOrWhiteSpace() && args != null)
                message = string.Format(message, args);

            return message;
        }

        /// <inheritdoc />
        public string GetMessage(string key, ICommandContext context, params object[] args) =>
            GetMessage(key, context.Player, args);
    }
}
