﻿using System;
using CGSGameManager.Core.Collections;
using CGSGameManager.Core.Covalence.Players;
using Network;

namespace CGSGameManager.Rust.Covalence
{
    public class PlayerManager : PlayerManager<RustUser, Connection>
    {
        internal Hash<ulong, RustUser> AuthQueue { get; }

        public PlayerManager() : base(ConvertFromConnection)
        {
            AuthQueue = new Hash<ulong, RustUser>();
        }

        #region Administration

        /// <inheritdoc />
        public override void Kick(RustUser player, string reason)
        {
            if (player == null || !player.IsConnected)
                return;

            if (player.Connection == null)
                return;

            player.EndSteamSession();
            Net.sv.Kick(player.Connection, reason ?? "No reason provided");
            player.Connection = null;
        }

        /// <inheritdoc />
        public override void Ban(RustUser player, string reason, TimeSpan duration)
        {
            if (player == null)
                return;

            if (ServerUsers.Is(player.SteamId, ServerUsers.UserGroup.Banned))
                return;

            ServerUsers.Set(player.SteamId, ServerUsers.UserGroup.Banned, player.UserName, reason ?? "No Reason");

            if (player.IsConnected)
                player.Kick(reason);

            ServerUsers.Save();
        }

        /// <inheritdoc />
        public override bool IsBanned(RustUser player) => ServerUsers.Is(player.SteamId, ServerUsers.UserGroup.Banned);

        /// <inheritdoc />
        public override void Unban(RustUser player)
        {
            if (IsBanned(player))
                ServerUsers.Remove(player.SteamId);
        }

        /// <inheritdoc />
        public override void SendMessageTo(RustUser player, string message) => player?.SendChatMessage(message);

        #endregion

        #region Static Helpers

        private static RustUser ConvertFromConnection(Connection player)
        {
            if (player != null && player.CgsPlayer is RustUser user)
                return user;

            var cgsPlayer = Interface.Core.CovalenceProvider.GetPlayerManager().Find(player.userid);

            if (cgsPlayer != null && cgsPlayer is RustUser user1)
            {
                user1.Connection = player;
                return user1;
            }

            user = new RustUser(player);

            var playermanager = (PlayerManager) Interface.Core.CovalenceProvider.GetPlayerManager();
            playermanager.All[user.SteamId] = user;
            return user;
        }

        #endregion
    }
}
