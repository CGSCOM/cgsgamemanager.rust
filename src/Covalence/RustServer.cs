﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using CGSGameManager.Core;
using CGSGameManager.Core.Covalence;
using ConVar;
using UnityEngine;
using Time = UnityEngine.Time;

namespace CGSGameManager.Rust.Covalence
{
    public sealed class RustServer : IServerManager
    {
        private readonly PlayerManager _playerManager = (PlayerManager)Interface.Core.CovalenceProvider.GetPlayerManager();

        #region Information

        /// <summary>
        ///     The public facing hostname of the server
        /// </summary>
        public string Hostname
        {
            get => Server.hostname;
            set => Server.hostname = value;
        }

        /// <summary>
        ///     The Current World Name
        /// </summary>
        public string MapName => World.Name;

        /// <summary>
        ///     The Current World Seed
        /// </summary>
        public uint MapSeed => World.Seed;

        /// <summary>
        ///     The Current World Size
        /// </summary>
        public uint MapSize => World.Size;

        /// <summary>
        ///     Current player count
        /// </summary>
        public int CurrentPlayers => BasePlayer.activePlayerList.Count;

        public int CurrentCgsPlayers => _playerManager.Connected.Count(a => !a.Value.Authenticated);

        public int CurrentSteamPlayers => _playerManager.Connected.Count(a => a.Value.Authenticated);

        public int CurrentOnlineAdminCount => _playerManager.Connected.Count(a => a.Value.IsAdmin || a.Value.IsCgsAdmin);

        /// <summary>
        ///     Max player capacity
        /// </summary>
        public int MaxPlayers => Server.maxplayers;

        /// <summary>
        ///     Current server run time
        /// </summary>
        public float Uptime => Time.realtimeSinceStartup;

        internal IPAddress cachedIP { private get; set; }

        /// <summary>
        ///     Public facing server IP:Port
        /// </summary>
        public IPEndPoint Endpoint => GetIPAddressInternal();

        #endregion

        #region Maintainence

        /// <summary>
        ///     Saves the server, and configs
        /// </summary>
        public void Save()
        {
            Server.save(null);

            var serverFolder = Server.GetServerFolder("cfg");
            var configString = ConsoleSystem.SaveToConfigString(true);
            File.WriteAllText(string.Concat(serverFolder, "/serverauto.cfg"), configString);
            ServerUsers.Save();
        }

        /// <summary>
        ///     Broadcasts a message to all players
        /// </summary>
        /// <param name="message"></param>
        public void BroadcastChat(string message)
        {
            if (string.IsNullOrEmpty(message)) return;
            ConsoleNetwork.BroadcastToAllClients("chat.add", 76561198337782098, message, 1.0);
        }

        /// <summary>
        ///     Starts the shutdown process
        /// </summary>
        public void Shutdown()
        {
            Application.Quit();
        }

        #endregion

        #region Helpers

        private IPEndPoint GetIPAddressInternal()
        {
            if (cachedIP != null) return new IPEndPoint(cachedIP, Server.port);
            if (!Interface.Core.ExtensionManager.ServerInitComplete) return null;

            if (Steamworks.SteamServer.PublicIp == null)
            {
                if (!IPAddress.TryParse(Server.ip, out var configIP)) return null;

                if (IsPrivateIP(configIP)) return null;

                return new IPEndPoint(configIP, Server.port);
            }

            cachedIP = Steamworks.SteamServer.PublicIp;
            if (CGSManager.DEBUGMODE) Interface.Core.LogInfo("[ServerManager] Cached IP from steam");
            return new IPEndPoint(Steamworks.SteamServer.PublicIp, Server.port);
        }

        private bool IsPrivateIP(IPAddress address)
        {
            if (address == null) address = IPAddress.Loopback;

            if (IPAddress.IsLoopback(address)) return true;

            var iparray = address.ToString().Split(new[] {'.'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(s => int.Parse(s)).ToArray();

            if (iparray[0] == 10 ||
                iparray[0] == 192 && iparray[1] == 168 ||
                iparray[0] == 172 && iparray[1] >= 16 && iparray[1] <= 31) return true;

            return false;
        }

        #endregion
    }
}
