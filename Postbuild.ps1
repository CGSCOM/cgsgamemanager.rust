﻿param (
    [Parameter(Mandatory=$true)][string]$outputdir,
    [Parameter(Mandatory=$true)][string]$outputzip
)

if (!(Test-Path "$outputdir")) {
        Write-Host "Could not find output directory $outputdir"
        exit 1
        if ($LASTEXITCODE -ne 0) { $Host.SetShouldExit($LASTEXITCODE) }
}

Compress-Archive "$outputdir" "$outputzip" -CompressionLevel Optimal -Force