﻿using System;
using System.Collections.Generic;
using System.Linq;
using CGSGameManager.Core.Covalence;

namespace CGSGameManager.Rust.Covalence.Commands
{
    public class RustCommand : ICommand
    {
        private readonly ConsoleSystem.Command[] _commands;
        private readonly string[] _aliases;
        private string _description;
        private readonly CommandCallback _callback;


        internal RustCommand(string category, string[] aliases, CommandCallback callback, string description)
        {
            if (aliases == null || aliases.Length == 0)
                throw new ArgumentNullException(nameof(aliases), "Command needs to have at lease one name");

            _callback = callback ?? throw new ArgumentNullException(nameof(callback), "Command needs a callback");

            if (string.IsNullOrWhiteSpace(category))
                category = "cgs";

            Category = category.ToLowerInvariant();
            _aliases = aliases.Where(a => !string.IsNullOrWhiteSpace(a)).Select(a => a.ToLowerInvariant()).ToArray();
            _description = description;

            _commands = new ConsoleSystem.Command[_aliases.Length];
            BuildNativeCommand(this);
        }

        /// <inheritdoc />
        public string ShortName => _aliases[0];

        /// <inheritdoc />
        public string Category { get; }

        /// <inheritdoc />
        public string FullName => $"{Category}.{ShortName}";

        /// <inheritdoc />
        public string Description
        {
            get => _description;
            set => SetDescriptionRecurse(value);
        }

        /// <inheritdoc />
        public IEnumerable<string> Aliases => _aliases;

        public void Call(ICommandContext context)
        {
            if (context == null)
                return;

            try
            {
                _callback.Invoke(context);
            }
            catch (Exception e)
            {
                context.Reply(e.Message);
            }
            finally
            {
                context.Dispose();
            }
        }

        private void OnConsoleCommand(ConsoleSystem.Arg arg)
        {
            if (arg == null)
                return;

            var provider = (CommandProvider) Interface.Core.CovalenceProvider.GetCommandProvider();
            provider.ProcessConsoleCommand(this, arg);
        }

        private void SetDescriptionRecurse(string description)
        {
            _description = description;

            foreach (var command in _commands)
            {
                command.Description = description;
            }
        }

        public bool Equals(ICommand command) => command != null && FullName == command.FullName;

        public void Dispose()
        {
            foreach (var command in _commands)
            {
                ConsoleSystem.Index.Server.Dict.Remove(command.FullName);
                if (command.Parent.StartsWith("global"))
                    ConsoleSystem.Index.Server.GlobalDict.Remove(command.FullName);
            }

            ConsoleSystem.Index.All = ConsoleSystem.Index.Server.Dict.Values.ToArray();
        }

        private static void BuildNativeCommand(RustCommand command)
        {
            if (command._commands == null || !command.Aliases.Any())
                return;

            for (var i = 0; i < command._aliases.Length; i++)
            {
                if (ConsoleSystem.Index.Server.Dict.TryGetValue($"{command.Category}.{command._aliases[i]}",
                    out var vanilla))
                {
                    if (vanilla.Variable)
                        continue;

                    ConsoleSystem.Index.Server.Dict.Remove(vanilla.FullName);
                }

                var native = new ConsoleSystem.Command()
                {
                    Parent = command.Category,
                    Description = command.Description,
                    Name = command._aliases[i],
                    FullName = $"{command.Category}.{command._aliases[i]}",
                    ServerUser = true,
                    ServerAdmin = true,
                    Client = true,
                    ClientInfo = false,
                    Variable = false,
                    Call = command.OnConsoleCommand
                };
                command._commands[i] = native;
                ConsoleSystem.Index.Server.Dict[native.FullName] = native;

                if (native.Parent.StartsWith("global", StringComparison.InvariantCultureIgnoreCase))
                    ConsoleSystem.Index.Server.GlobalDict[native.FullName] = native;
            }

            ConsoleSystem.Index.All = ConsoleSystem.Index.Server.Dict.Values.ToArray();
        }
    }
}
