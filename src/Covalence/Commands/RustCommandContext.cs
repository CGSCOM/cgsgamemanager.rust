﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using CGSGameManager.Core.Covalence;

namespace CGSGameManager.Rust.Covalence.Commands
{
    public class RustCommandContext : ICommandContext
    {
        public ConsoleSystem.Arg NativeCallback { get; internal set; }

        public string Command { get; private set; }

        public string[] Arguments { get; private set; }

        public ICgsPlayer Player { get; private set; }

        public bool IsServer => Player is IServerPlayer;

        public bool IsPlayer => !IsServer;

        public void Reply(string message)
        {
            if (string.IsNullOrWhiteSpace(message))
                return;

            if (IsPlayer)
            {
                Player.Reply(message);
                return;
            }
            message = Regex.Replace(message, @"<[^>]*>", string.Empty, RegexOptions.IgnoreCase);

            if (NativeCallback != null)
                NativeCallback.ReplyWith(message);
            else
                Player.Reply($"[CMD: {Command}] {message}");
        }

        public void Reply(string message, params object[] args)
        {
            if (string.IsNullOrWhiteSpace(message))
                return;

            if (args == null || args.Length == 0)
            {
                Reply(message);
                return;
            }

            Reply(string.Format(message, args));
        }

        internal RustCommandContext(ICgsPlayer user, RustCommand command, string[] args)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command), "Base Command is required to create a instance");
            Player = user ?? throw new ArgumentNullException(nameof(user), "A user needs to be provided");
            Command = command.FullName;
            Arguments = args ?? new string[0];
        }

        internal RustCommandContext(ICgsPlayer user, RustCommand command,  ConsoleSystem.Arg arg)
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command), "Base Command is required to create a instance");
            Player = user ?? throw new ArgumentNullException(nameof(user), "A user needs to be provided");

            NativeCallback = arg ?? throw new ArgumentNullException(nameof(arg), "ConsoleInstance is required to create context");
            Command = command.FullName;
            Arguments = ExtractArgs(arg);
        }

        public void Dispose()
        {
            NativeCallback = null;
            Command = null;
            Arguments = null;
            Player = null;
        }

        private static string[] ExtractArgs(ConsoleSystem.Arg arg)
        {
            if (arg == null) return new string[0];

            var alist = new List<string>();

            var i = 0;

            while (arg.HasArgs(++i)) alist.Add(arg.GetString(i - 1));

            return alist.ToArray();
        }
    }
}
